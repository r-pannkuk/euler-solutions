#include "EulerExcercises.h"
#include "Helper.h"
#include <stdlib.h>
#include <string.h>

int* ParseIntTriangleRow(char* row, unsigned length);
int ParseIntFromRow(char* str, unsigned numDigits);
int BestSum(int** row, unsigned currRow, unsigned currIndex, unsigned numRows);
int BestSum2(int** row, unsigned currRow, unsigned currIndex, unsigned numRows, int** sums);

long long MaximumPathSum(char** triangle, unsigned numRows)
{
  int** rows = (int**)malloc(numRows * sizeof(int*));
  int** sums = (int**)malloc(numRows * sizeof(int*));

  for(unsigned i = 0; i < numRows; ++i)
  {
    rows[i] = ParseIntTriangleRow(triangle[i], i + 1);
    sums[i] = (int*)calloc(i + 1, sizeof(int));
  }

  memcpy(sums[numRows - 1], rows[numRows - 1], sizeof(int) * numRows - 1);

  long long maxSum = BestSum2(rows, 0, 0, numRows, sums);
  
  for(unsigned i = 0; i < numRows; ++i)
  {
    free(rows[i]);
    free(sums[i]);
  }

  free(rows);
  free(sums);

  return maxSum;
}

int* ParseIntTriangleRow(char* row, unsigned numInts)
{
  int* parsedInts = (int*)malloc(sizeof(int) * numInts);

  for(unsigned i = 0; i < numInts; ++i)
    parsedInts[i] = ParseIntFromRow(row + i * 3, 2);

  return parsedInts;
}


int ParseIntFromRow(char* str, unsigned numDigits)
{
  char* buffer = (char*)malloc(sizeof(char) * (numDigits + 1));

  memmove(buffer, str, numDigits);

  buffer[numDigits] = 0;

  int newInt = atoi(buffer);

  free(buffer);

  return newInt;
}


int BestSum(int** row, unsigned currRow, unsigned currIndex, unsigned numRows)
{
  if(currRow == numRows - 1)
    return row[currRow][currIndex];

  return row[currRow][currIndex] + 
    max(BestSum(row, currRow + 1, currIndex, numRows), BestSum(row, currRow + 1, currIndex + 1, numRows));
}

int BestSum2(int** row, unsigned currRow, unsigned currIndex, unsigned numRows, int** sums)
{
  if(sums[currRow][currIndex] > 0)
    return sums[currRow][currIndex];

  sums[currRow][currIndex] = row[currRow][currIndex] +
    max(BestSum2(row, currRow + 1, currIndex, numRows, sums), 
        BestSum2(row, currRow + 1, currIndex + 1, numRows, sums));

  return sums[currRow][currIndex];
}