#include "EulerExcercises.h"
#include "Helper.h"

int DigitInUse(unsigned char digit, large_number* number);

unsigned long long NthLexicographicPermutation(unsigned numDigits, unsigned long n)
{
  large_number number = LargeNumber();
  unsigned long numPermutations = 0;
  unsigned long currentIterations;

  number.numDigits = 1;
  
  while(numPermutations < n - 1)
  {
    currentIterations = 1;

    // n!
    for(unsigned i = 1; i < numDigits; ++i)
      currentIterations *= i;

    while(numPermutations + currentIterations < n)
    {
      while(DigitInUse(++number.d[number.numDigits - 1], &number) == 0);

      numPermutations += currentIterations;
    }

    ++number.numDigits;
    --numDigits;
  }

  unsigned int unusedDigit = '0';

  while(numDigits > 0)
  {
    while(DigitInUse(unusedDigit, &number) == 0)
      ++unusedDigit;

    number.d[number.numDigits++] = unusedDigit;
    --numDigits;
  }

  --number.numDigits;

  return (unsigned long long)LargeNumberToLong(&number);
}


int DigitInUse(unsigned char digit, large_number* number)
{
  for(unsigned char i = 0; i < number->numDigits - 1; ++i)
  {
    if(number->d[i] == digit)
      return 0;
  }

  return 1;
}