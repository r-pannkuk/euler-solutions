#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>
#include <stdlib.h>

long long SumOfProperDivisors(long long n);
long long SumOfProperDivisorsCache(long long n, long long *cache, int* sieve);

long long SumOfAmicableNumbers(unsigned n)
{
  long long sum = 0;
  long long d_i;
  long long d_d_i;

  // Limit for our cache is the sum of (n) + (n/2) + (n/3) + ...
  // We calculate that to find the maximum size of our cache and sieve

  long long root = (long long)floor(sqrt((double)n));
  size_t limit = n;
  
  for(size_t i = 2; i <= root; ++i)
    limit += n / i;
  
  long long* cache = (long long *)calloc(limit, sizeof(long long));
  int* sieve = (int*)calloc(limit, sizeof(int));

  SieveOfEratosthenesOptimized(limit, sieve);

  for(unsigned i = 1; i < n; ++i)
  {
    d_i = SumOfProperDivisorsCache(i, cache, sieve);
    d_d_i = SumOfProperDivisorsCache(d_i, cache, sieve);

    if(d_i != i && d_d_i == i)
      sum += i;
  }

  free(sieve);
  free(cache);

  return sum;
}