#include "EulerExcercises.h"
#include "Helper.h"

long long FactorialDigitSum(unsigned n)
{
  large_number f = Factorial(n);
  long long sum = 0;

  for(int i = 0; i < f.numDigits; ++i)
    sum += f.d[i] - '0';

  return sum;
}