#include "EulerExcercises.h"
#include "Helper.h"
#include <stdlib.h>
#include <math.h>

unsigned long long SumOfNonAbundantIntegers(unsigned limit)
{
  // Any integer greater than 28123 can be written as the sum
  // of two abundant numbers
  if(limit > 28123)
    limit = 28123;

  unsigned long long sum = 0;

  size_t cacheSize = limit;
  unsigned root = (unsigned)floor(sqrt((double)limit));

  char* abundancies = (char*)calloc(limit + 1, sizeof(char));

  for(unsigned i = 1; i <= limit; ++i)
    abundancies[i] = (SumOfProperDivisors(i) > i) ? 1 : 0;

  unsigned currentValue;

  // Twelve is the smallest abundant number
  for(unsigned i = 1; i <= limit; ++i)
  {
    currentValue = i;

    for(unsigned j = 12; j <= i; ++j)
    {
      if(abundancies[j] == 0)
        continue;

      if(abundancies[i - j] == 1)
      {
        currentValue = 0;
        break;
      }
    }

    sum += currentValue;
  }

  free(abundancies);

  return sum;
}