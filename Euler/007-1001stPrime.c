#include "EulerExcercises.h"
#include "Helper.h"

long long NthPrime(unsigned n)
{
  unsigned primesFound = 1;
  long long iterator = 1;

  while(primesFound < n)
  {
    iterator += 2;

    if(IsPrime(iterator))
    {
      ++primesFound;
    }
  }

  return iterator;
}