#include "Helper.h"

#pragma once

// 1) Multiples of 3 and 5
int NumMultiples(int firstValue, int secondValue, int lowerBound, int upperBound);

// 2) Even Fibonacci Numbers
unsigned SumOfEvenFibonacci(unsigned limit);

// 3) Largst Prime Factor
long long LargestPrimeFactor(long long number);

// 4) Largest Palindrome Product
long LargestPalindromeProduct(unsigned digits);

// 5) Smallest Multiple of All Numbers Between Bounds
long long SmallestMultiple(unsigned lowerBound, unsigned upperBound);

// 6) Difference between square of sums (1 + 2 + ...)^2 and sum of square (1^2 + 2^2 ...)
long long SumSquareDifference(unsigned upperBound);

// 7) Find the 10001st Prime Number
long long NthPrime(unsigned n);

// 8) Largest Product in a Series
long long LargestProductInSeries(char* number, int length, int adjacentDigits);

// 9) Find a*b*c where a + b + c = n (Pythagorean Triplet)
long long SpecialPythagoreanTriplet(int n);

// 10) Find the sum of all primes below two million
long long SummationOfPrimes(int upperLimit);
long long SummationOfPrimes2(int upperLimit);
long long SummationOfPrimes3(int upperLimit);

// 11) Find the product of adjacent numbers in the given grid
long long LargestProductInGrid(int *grid, int width, int height, int numAdjacent);

// 12) Find the first triangular number to have the given number of divisors
long long DivisibleTriangularNumber(unsigned numDivisors);

// 13) First 10 digits of the sum of a large number
long long FirstDigitsOfSum(char* numberString, unsigned lines, unsigned digitsPerLine, unsigned numFirstDigits);

// 14) Find the number returning the longest Collatz sequence under 1,000,000
long long LongestCollatzSequence(long long limit);

// 15) How many paths are there in a 20 x 20 grid moving only in one direction horizontally or vertically?
long long LatticePaths(unsigned int width, unsigned int height);

// 16) Find the sum of the digits of 2^1000
long long PowerDigitSum(int base, int exponent);

// 17) Count the number of letters used in spelling out every number from 1 to 1000
long long NumberLetterCounts(int lowerBound, int upperBound);

// 18) Find the maximum sum using a single path of numbers from the number triangle
long long MaximumPathSum(char** triangle, unsigned numRows);

// 19) Find all of the sundays which occurred on the first of a month in the 20th century
int CountingSundays(date * start, date * end);

// 20) Find the sum of the digits in the number represented by 100!
long long FactorialDigitSum(unsigned n);

// 21) Find the sum of amicable numbers (numbers where d(a) = b and d(b) = a, a != b) where
//     d(n) is the sum of proper divisors of n. 
long long SumOfAmicableNumbers(unsigned limit);

// 22) Using the file 022_names.txt, sort the list of names, and then find the sum of the characters
//     making up the name, and multiply each name by it's position of the list.  Find the sum. 
long long NamesScores(char* filename);

// 23) Find the sum of all non-abundant positive integers
unsigned long long SumOfNonAbundantIntegers(unsigned limit);

// 24) What is the millionth lexicographic permutation of the digits 0,1,2,3,4,5,6,7,8, and 9?
unsigned long long NthLexicographicPermutation(unsigned numDigits, unsigned long n);

// 25) What is the index of the first term in the Fibonacci Sequence to contain 1000 digits
unsigned NthDigitFibonacciNumberIndex(unsigned numDigits);

// 26) Find the value of d, d < 1000, where 1/d has the longest reciprocal cycle
unsigned LongestReciprocalCycle(unsigned limit);

// 27) Find the product of coefficients for the quadratic expression that produces the maximum
//     number of primes for consecutive values of n, starting with n = 0
long long QuadraticPrimes(unsigned a, unsigned b);

// 28) Find the sum of the diagonals in a 1001 by 1001 formed spiral
unsigned long long SumOfDiagonals(unsigned long n);

// 29) Find the number of distinct terms for a^b, 2 <= a <= 100, 2 <= b <= 100
unsigned DistinctPowers(unsigned minA, unsigned maxA, unsigned minB, unsigned maxB);