#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>
#include <stdlib.h>

long long SummationOfPrimes(int upperLimit)
{
  long long sum = 2;
  int *sieve = (int*)malloc(upperLimit);
  int root = (int)ceil(sqrt(upperLimit));

  for(int i = 3; i < upperLimit; i += 2)
  {
    if(IsPrime(i))
      sum += i;
  }

  return sum;
}

long long SummationOfPrimes2(int upperLimit)
{
  long long sum = 2;
  char* sieve = SieveOfEratosthenes(upperLimit);

  for(int i = 3; i < upperLimit; i += 2)
  {
    if(sieve[i] == FALSE)
    {
      sum += i;
    }
  }

  free(sieve);
  return sum;
}

long long SummationOfPrimes3(int upperLimit)
{
  long long sum = 2;
  int sieveBound = 0;
  char* sieve = SieveOfEratosthenesOptimized(upperLimit, &sieveBound);

  for(int i = 1; i < sieveBound; ++i)
  {
    if(sieve[i] == FALSE)
    {
      sum += 2 * i + 1;
    }
  }

  free(sieve);
  return sum;
}