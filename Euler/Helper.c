#include "Helper.h"

#include <math.h>
#include <stdlib.h>

long long GetNextPrime(long long startingPoint)
{
  long long next = startingPoint;

  while(!IsPrime(++next));

  return next;
}

int IsPrime(long long number)
{
  // 1 cannot be a prime number
  if(number == 1)
    return 0;
  // Both 2 and 3 are prime numbers
  if(number < 4)
    return 1;
  // Any number divisible by 2
  if(number % 2 == 0)
    return 0;
  // Excluded 4, 6, 8
  if(number < 9)
    return 1;
  // Any number divisible by 3
  if(number % 3 == 0)
    return 0;

  // sqrt(n) rounded to largest integer so root * root >= n
  long long root = (long long)ceil(sqrt((double)number));

  // Starting at 5 since checks have solved others
  // Can increment by 6 since all primes after 3 can
  // be written as: 
  //    6k +/- 1
  for(long long i = 5; i <= root; i += 6)
  {
    if(number % i == 0)
      return 0;
    if(number % (i + 2) == 0)
      return 0;
  }

  return 1;
}

int NthDigit(long number, unsigned index)
{
  static unsigned powersOf10[] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 1000000, 10000000, 100000000 };
  static unsigned length = 10;

  if(index < length)
    return (number / powersOf10[index]) % 10;


  while(index--)
    number /= 10;

  return number % 10;
}

unsigned NumDigits(long long number)
{
  return (unsigned)log10((double)number) + 1;
}


long long EuclideanAlgorithm(long long a, long long b)
{
  long long largest, smallest;

  if(a > b)
  {
    largest = a;
    smallest = b;
  }
  else 
  {
    largest = a;
    smallest = b;
  }

  while(smallest != 0)
  {
    long long temp = smallest;
    smallest = largest % smallest;
    largest = temp;
  }

  return largest;
}

int IsPalindrome(long number)
{
  long original = number;
  long reverse = 0;

  while(number != 0)
  {
    reverse = reverse * 10 + number % 10;
    number /= 10;
  }

  return reverse == original;
}


long long LeastCommonMultiple(long long a, long long b)
{
  if(a == 1)
    return b;
  if(b == 1)
    return a;

  return (a * b) / EuclideanAlgorithm(a, b);
}


int CharToInt(char digit)
{
  return digit - '0';
}

char* SieveOfEratosthenes(int upperLimit)
{
  // All numbers from 2 to upperLimit
  // Initializing to 0, where 0 means a prime number, 1 means non-prime
  // Using 0 instead of 1 because calloc is faster than doing a memset in this way
  char *sieve = (char*)calloc(upperLimit, sizeof(char));
  int root = (int)ceil(sqrt(upperLimit));

  for(int i = 4; i < upperLimit; i += 2)
  {
    sieve[i] = TRUE;
  }

  for(int i = 3; i < root; i += 2)
  {
    if(sieve[i] == FALSE)
    {
      for(int j = i * i; j < upperLimit; j += 2 * i)
      {
        sieve[j] = TRUE;
      }
    }
  }

  return sieve;
}

char* SieveOfEratosthenesOptimized(int upperLimit, int* sieveBound)
{
  // We can minimize the memory overhead of the array by discriminating
  // even numbers other than 2. 
  *sieveBound = (upperLimit - 1) / 2;
  char *sieve = (char*)calloc(*sieveBound, sizeof(char));
  int root = ((int)ceil(sqrt(upperLimit)) - 1) / 2;

  // Now our array is indexed by 2i + 1, which corresponds to every
  // odd value above 2. 
  for(int i = 1; i < root; ++i)
  {
    if(sieve[i] == FALSE)
    {
      for(int j = 2 * i * (i + 1); j < *sieveBound; j += 2 * i + 1)
      {
        sieve[j] = TRUE;
      }
    }
  }

  // When using this array for calculations, we need to do arithmetic
  // to determine the actual value from it's index.  That is:
  //    sieve[i] = 2 * i + 1
  return sieve;
}

large_number LargeNumber()
{
  large_number value;

  for(unsigned i = 0; i < MAX_DIGITS; ++i)
    value.d[i] = '0';

  value.numDigits = 0;

  return value;
}

long long LargeNumberToLong(large_number* n)
{
  long long value = 0;

  for(int i = (int)n->numDigits - 1, exponent = 0; i >= 0; --i, ++exponent)
    value += (long long)pow(10, exponent) * (n->d[i] - '0');

  return value;
}

large_number StringToLargeNumber(char* string, unsigned length)
{
  large_number value;

  for(unsigned i = 0; i < length; ++i)
  {
    value.d[i] = string[i];
  }

  value.numDigits = length;

  return value;
}

int Compare(large_number *lhs, large_number *rhs)
{
  if(lhs->numDigits != rhs->numDigits)
    return (lhs->numDigits > rhs->numDigits) ? 1 : -1;

  for(unsigned i = 0; i < lhs->numDigits; ++i)
  {
    if(lhs->d[i] > rhs->d[i])
      return 1;
    else if(lhs->d[i] < rhs->d[i])
      return -1;
  }

  return 0;
}

large_number Trim(large_number *n)
{
  large_number result = *n;

  if(n->numDigits != MAX_DIGITS && n->numDigits != 0)
    return result;

  unsigned numBufferDigits;
  for(numBufferDigits = 0; result.d[numBufferDigits] == '0'; ++numBufferDigits);

  for(unsigned i = 0; i < MAX_DIGITS - numBufferDigits; ++i)
    result.d[i] = result.d[i + numBufferDigits];

  result.numDigits = MAX_DIGITS - numBufferDigits;

  return result;
}

large_number Add(large_number* lhs, large_number* rhs)
{
  unsigned max_length = (lhs->numDigits > rhs->numDigits) ? lhs->numDigits : rhs->numDigits;
  large_number sum = LargeNumber();
  int carry = 0;
  int addend = 0;

  unsigned sumIndex = MAX_DIGITS - 1;

  for(unsigned i = 0; i < max_length; ++i, --sumIndex)
  {
    addend += carry;

    if(i < lhs->numDigits)
      addend += lhs->d[lhs->numDigits - 1 - i] - '0';
    if(i < rhs->numDigits)
      addend += rhs->d[rhs->numDigits - 1 - i] - '0';
    
    carry = addend / 10;
    addend %= 10;

    sum.d[sumIndex] = '0' + addend;

    addend = 0;
  }

  sum.d[sumIndex] += carry;

  return Trim(&sum);
}

large_number Subtract(large_number * lhs, large_number * rhs)
{
  large_number difference = LargeNumber();

  unsigned lengthDiff = lhs->numDigits - rhs->numDigits;

  for(unsigned i = 0, differenceIndex = MAX_DIGITS - 1; i < lhs->numDigits; ++i, --differenceIndex)
  {
    if(i < lengthDiff)
      difference.d[differenceIndex] += lhs->d[lhs->numDigits - 1 - i] - rhs->d[rhs->numDigits - 1 - i - lengthDiff];
    else
      difference.d[differenceIndex] += lhs->d[lhs->numDigits - 1 - i] - '0';

    if(difference.d[differenceIndex] < '0')
    {
      difference.d[differenceIndex] += 10;
      --difference.d[differenceIndex - 1];
    }
  }

  return Trim(&difference);
}

large_number Scalar(large_number * lhs, unsigned long long rhs)
{
  if(rhs == 1)
    return *lhs;

  unsigned long long carry = 0;
  unsigned long long temp;

  unsigned productIndex = MAX_DIGITS - 1;
  large_number product = LargeNumber();

  for(unsigned i = 0; i < lhs->numDigits; ++i, --productIndex)
  {
    temp = rhs * (lhs->d[lhs->numDigits - 1 - i] - '0') + (product.d[productIndex] - '0');
    product.d[productIndex] = temp % 10 + '0';
    temp /= 10;

    for(unsigned j = productIndex - 1; j >= 0 && temp > 0; --j, temp /= 10)
    {
      product.d[j] += temp % 10;

      for(unsigned k = j; k >= 0 && product.d[k] >= '0' + 10; --k)
      {
        product.d[k] = (product.d[k] - '0') % 10 + '0';
        ++product.d[k - 1];
      }
    }
  }

  return Trim(&product);
}

large_number Multiply(large_number * lhs, large_number * rhs)
{
  unsigned long long carry = 0;
  unsigned long long temp;

  unsigned productIndex;
  large_number product = LargeNumber();

  for(unsigned i = 0; i < lhs->numDigits; ++i)
  {
    productIndex = MAX_DIGITS - i - 1;

    for(unsigned j = 0; j < rhs->numDigits; ++j, --productIndex)
    {
      temp =  (rhs->d[rhs->numDigits - 1 - j] - '0') * 
              (lhs->d[lhs->numDigits - 1 - i] - '0') + 
              (product.d[productIndex] - '0');
      product.d[productIndex] = temp % 10 + '0';
      temp /= 10;

      for(unsigned k = productIndex - 1; k >= 0 && temp > 0; --k, temp /= 10)
      {
        product.d[k] += temp % 10;

        for(unsigned l = k; l >= 0 && product.d[l] >= '0' + 10; --l)
        {
          product.d[l] = (product.d[l] - '0') % 10 + '0';
          ++product.d[l - 1];
        }
      }
    }
  }

  return Trim(&product);
}

large_number Divide(large_number * lhs, large_number * divisor)
{
  large_number one;
  one.numDigits = 1;
  one.d[0] = '1';

  large_number result = LargeNumber();
  large_number quotient = *lhs;

  while(Compare(&quotient, divisor) >= 0)
  {
    quotient = Subtract(&quotient, divisor);
    result = Add(&result, &one);
  }

  return Trim(&result);
}

void FactorialRecursion(large_number *n, unsigned long scalar)
{
  if(scalar == 0 || scalar == 1)
    return;

  FactorialRecursion(n, scalar - 1);

  *n = Scalar(n, scalar);
}


large_number Factorial(unsigned long n)
{
  large_number factorial = LargeNumber();
  factorial.d[0] = 1 + '0';
  factorial.numDigits = 1;

  for(unsigned i = 2; i < n; ++i)
    factorial = Scalar(&factorial, i);

  return factorial;
}

long long Binomial(unsigned int n, unsigned int k)
{
  long long c = 1;

  if(k > n - k)
    k = n - k;

  for(unsigned i = 1; i <= k; ++i, --n)
    c = c / i * n + c % i * n / i;

  return c;
}

unsigned DayOfWeek(date* date)
{
  // Gauss's Algorithm
  return (unsigned)(date->day + (2.6 * (date->month + 1) - 0.2) + 5 * (date->year % 4) + 
          4 * (date->year % 100) + 6 * (date->year % 400)) 
    % 7;
}


long long SumOfProperDivisors(long long n)
{
  long long root = (long long)floor(sqrt((double)n));
  long long sum = 1;

  for(long long i = 2; i <= root; ++i)
  {
    if(n % i == 0)
    {
      sum += i;

      if(n / i != i)
        sum += n / i;
    }
  }

  return sum;
}

long long SumOfProperDivisorsCache(long long n, long long* cache, int* sieve)
{
  if(sieve[n] == 1)
    cache[n] = 1;
  if(cache[n] != 0)
    return cache[n];

  long long root = (long long)floor(sqrt((double)n));
  cache[n] = 1;

  for(long long i = 2; i <= root; ++i)
  {
    if(n % i == 0)
    {
      cache[n] += i;

      if(n / i != i)
        cache[n] += n / i;
    }
  }

  return cache[n];
}


// Calculates [base^power % modulus] using an efficient modulo of squares algorithm
// See: http://stackoverflow.com/questions/8496182/calculating-powa-b-mod-n
unsigned long long ModuloPow(unsigned long long base,
                             unsigned long long power,
                             unsigned long long modulus)
{
  base %= modulus;
  unsigned long long result = 1;

  while(power > 0)
  {
    if(power & 1)
      result = (result * base) % modulus;

    base = (base * base) % modulus;
    power >>= 1;
  }

  return result;
}