#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>
#include <stdlib.h>

unsigned LongestReciprocalCycle(unsigned limit)
{
  unsigned period;
  unsigned largestN;
  unsigned longestCycle = 0;

  char* sieve = SieveOfEratosthenes(limit + 1);

  for(unsigned n = limit; n > 1; --n)
  {
    // Continue on non-primes
    if(sieve[n] == 1)
      continue;
    if(longestCycle >= n - 1)
      break;
    
    period = 0;

    // Applying Fermat's Little Theorem:
    //   1/d has a cycle of n digits if 10^n - 1 % d == 0
    //   Prime numbers can yield up to d - 1 repeating digits
    while((unsigned long long)ModuloPow(10, ++period, n) != 1);

    if(period > longestCycle)
    {
      longestCycle = period;
      largestN = n;
    }
  }

  free(sieve);

  return largestN;
}