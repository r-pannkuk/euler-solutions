#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>

unsigned NumDivisors(long long triangularNumber);
unsigned NumDivisors2(long long triangularNumber, int start);

long long DivisibleTriangularNumber(unsigned target) 
{
  if(target == 1)
    return 1;

  long long triangularNumber = 0;
  unsigned numDivisors = 0;
  unsigned previousDivisors = 1;

  // Since a triangular number is defined by n * (n+1) / 2, we can 
  // factor the number of divisors to n and n + 1
  // The product of divisors will equal how many divisors there are, since 
  // no two consecutive numbers share any divisor other than 1. 
  for(int iterator = 1; previousDivisors * numDivisors < target; ++iterator)
  {
    previousDivisors = numDivisors;

    // Triangle numbers can be written as: 
    //    T_n = 1/2 * n * (n + 1)
    triangularNumber = iterator * (iterator + 1) / 2;

    if(iterator % 2 == 0)
      numDivisors = NumDivisors2(iterator + 1, 2);
    else
      numDivisors = NumDivisors2((iterator + 1) / 2, 2); 
  }

  return triangularNumber;
}


unsigned NumDivisors(long long triangularNumber)
{
  if(triangularNumber == 1)
    return 1;

  int root = (int)sqrt((double)triangularNumber);

  // We can start at 2 because we know 1 and n are the divisors of n
  int numDivisors = 2;

  for(int i = 2; i < root; ++i)
  {
    if(triangularNumber % i == 0)
      numDivisors += 2;
  }

  return numDivisors; 
}


unsigned NumDivisors2(long long triangularNumber, int start)
{
  if(triangularNumber == 1)
    return 1;

  // Any given number can be represented by it's prime factorization: 
  //    N = p^a * q^b * r^c...

  // The function D(n) can be used to denote the number of divisors: 
  //    D(n) = (a+1)(b+1)(c+1)...

  int root = (int)ceil(sqrt((double)triangularNumber));
  int exponent = 1;

  for(int i = start; i < root; ++i)
  {
    if(triangularNumber % i == 0)
    {
      int exponent = 1;

      // Every iteration is another factor of the value, so 8 would be 2^3
      while(triangularNumber % i == 0)
      {
        triangularNumber /= i;
        ++exponent;
      }

      return NumDivisors2(triangularNumber, i + 1) * exponent;
    }
  }

  return 2;
}