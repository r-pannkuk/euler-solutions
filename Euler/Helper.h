#pragma once

#define TRUE 1
#define FALSE 0
#define MAX_DIGITS 0xffff

typedef struct {
  unsigned short numDigits;
  char d[MAX_DIGITS];
} large_number;

typedef struct {
  unsigned int year;    
  unsigned char month;  // Months since January [0 - 11]
  unsigned char day;    // Day of the month [1 - 31]
} date;

int IsPrime(long long number);
long long GetNextPrime(long long startingPoint);
unsigned NumDigits(long long number);
int NthDigit(long number, unsigned index);
long long EuclideanAlgorithm(long long a, long long b);
long long LeastCommonMultiple(long long a, long long b);
int IsPalindrome(long number);
int CharToInt(char digit);
char* SieveOfEratosthenes(int upperLimit);
char* SieveOfEratosthenesOptimized(int upperLimit, int* sieveBound);
large_number LargeNumber();
long long LargeNumberToLong(large_number* n);
large_number StringToLargeNumber(char* string, unsigned splitDigit);
large_number Add(large_number* lhs, large_number* rhs);
large_number Scalar(large_number * lhs, unsigned long long rhs);
large_number Multiply(large_number * lhs, large_number * rhs);
large_number Divide(large_number * lhs, large_number * divisor);
large_number Factorial(unsigned long n);
long long Binomial(unsigned int width, unsigned int height);
unsigned DayOfWeek(date* date);
long long SumOfProperDivisors(long long n);
long long SumOfProperDivisorsCache(long long n, long long *cache, int* sieve);
unsigned long long ModuloPow(unsigned long long base,
                             unsigned long long power, 
                             unsigned long long modulus);