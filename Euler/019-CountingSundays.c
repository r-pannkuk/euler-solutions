#include "EulerExcercises.h"
#include "Helper.h"
#include <time.h>

#define MONTHS 12

int CountingSundays(date * start, date * end)
{  
  date s = *start;
  date e = *end;

  unsigned numSundays = 0;

  while(s.year < e.year)
  {
    if(DayOfWeek(&s) == 0)
      ++numSundays;

    if(++s.month == MONTHS)
    {
      s.month = 0;
      ++s.year;
    }
  }

  return numSundays;
}
