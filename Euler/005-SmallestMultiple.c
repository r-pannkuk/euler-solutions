#include "EulerExcercises.h"
#include "Helper.h"

long long SmallestMultiple(unsigned lowerBound, unsigned upperBound)
{
  long long lcm = 1;

  for(unsigned i = lowerBound; i <= upperBound; ++i)
  {
    lcm = LeastCommonMultiple(i, lcm);
  }

  return lcm;
}