#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <stdint.h> // portable: uint64_t   MSVC: __int64 

typedef struct timeval {
  long tv_sec;
  long tv_usec;
} timeval;

int GetTimeOfDay(struct timeval * tp, struct timezone * tzp);
double Duration(struct timeval * start, struct timeval * end);