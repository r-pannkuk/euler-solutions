#pragma once

#include "EulerExcercises.h"
#include "Helper.h"
#include "Timer.h"
#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define NUM_EXCERCISES 451

void(*Excercises[NUM_EXCERCISES])(void);

void TestAll(void)
{
  struct timeval start, end;

  for(int i = 1; i <= NUM_EXCERCISES; ++i)
    if(Excercises[i] != 0)
    {
      GetTimeOfDay(&start, NULL);
      Excercises[i]();
      GetTimeOfDay(&end, NULL);
      printf("Excercise %3d: %2.3fs\n", i, Duration(&start, &end));
    }
}

void Test001(void) { assert(NumMultiples(3, 5, 1, 1000) == 233168); }
void Test002(void) { assert(SumOfEvenFibonacci(4000000) == 4613732); }
void Test003(void) { assert(LargestPrimeFactor(600851475143) == 6857); }
void Test004(void) { assert(LargestPalindromeProduct(3) == 906609); }
void Test005(void) { assert(SmallestMultiple(1, 20) == 232792560); }
void Test006(void) { assert(SumSquareDifference(100) == 25164150); }
void Test007(void) { assert(NthPrime(10001) == 104743); }
void Test008(void) { assert(LargestProductInSeries(
    "73167176531330624919225119674426574742355349194934"
    "96983520312774506326239578318016984801869478851843"
    "85861560789112949495459501737958331952853208805511"
    "12540698747158523863050715693290963295227443043557"
    "66896648950445244523161731856403098711121722383113"
    "62229893423380308135336276614282806444486645238749"
    "30358907296290491560440772390713810515859307960866"
    "70172427121883998797908792274921901699720888093776"
    "65727333001053367881220235421809751254540594752243"
    "52584907711670556013604839586446706324415722155397"
    "53697817977846174064955149290862569321978468622482"
    "83972241375657056057490261407972968652414535100474"
    "82166370484403199890008895243450658541227588666881"
    "16427171479924442928230863465674813919123162824586"
    "17866458359124566529476545682848912883142607690042"
    "24219022671055626321111109370544217506941658960408"
    "07198403850962455444362981230987879927244284909188"
    "84580156166097919133875499200524063689912560717606"
    "05886116467109405077541002256983155200055935729725"
    "71636269561882670428252483600823257530420752963450", 1000, 13) == 23514624000); }
void Test009(void) { assert(SpecialPythagoreanTriplet(1000) == 31875000); }
void Test010(void) { assert(SummationOfPrimes3(2000000) == 142913828922); }\
void Test011(void) {
  int grid[20 * 20] = 
  {
       8, 2,22,97,38,15, 0,40, 0,75, 4, 5, 7,78,52,12,50,77,91, 8,
      49,49,99,40,17,81,18,57,60,87,17,40,98,43,69,48, 4,56,62, 0,
      81,49,31,73,55,79,14,29,93,71,40,67,53,88,30, 3,49,13,36,65,
      52,70,95,23, 4,60,11,42,69,24,68,56, 1,32,56,71,37, 2,36,91,
      22,31,16,71,51,67,63,89,41,92,36,54,22,40,40,28,66,33,13,80,
      24,47,32,60,99, 3,45, 2,44,75,33,53,78,36,84,20,35,17,12,50,
      32,98,81,28,64,23,67,10,26,38,40,67,59,54,70,66,18,38,64,70,
      67,26,20,68, 2,62,12,20,95,63,94,39,63, 8,40,91,66,49,94,21,
      24,55,58, 5,66,73,99,26,97,17,78,78,96,83,14,88,34,89,63,72,
      21,36,23, 9,75, 0,76,44,20,45,35,14, 0,61,33,97,34,31,33,95,
      78,17,53,28,22,75,31,67,15,94, 3,80, 4,62,16,14, 9,53,56,92,
      16,39, 5,42,96,35,31,47,55,58,88,24, 0,17,54,24,36,29,85,57,
      86,56, 0,48,35,71,89, 7, 5,44,44,37,44,60,21,58,51,54,17,58,
      19,80,81,68, 5,94,47,69,28,73,92,13,86,52,17,77, 4,89,55,40,
      04,52, 8,83,97,35,99,16, 7,97,57,32,16,26,26,79,33,27,98,66,
      88,36,68,87,57,62,20,72, 3,46,33,67,46,55,12,32,63,93,53,69,
       4,42,16,73,38,25,39,11,24,94,72,18, 8,46,29,32,40,62,76,36,
      20,69,36,41,72,30,23,88,34,62,99,69,82,67,59,85,74, 4,36,16,
      20,73,35,29,78,31,90, 1,74,31,49,71,48,86,81,16,23,57, 5,54,
       1,70,54,71,83,51,54,69,16,92,33,48,61,43,52, 1,89,19,67,48
  };
  assert(LargestProductInGrid(grid, 20, 20, 4) == 70600674);
}
void Test012(void) { assert(DivisibleTriangularNumber(500) == 76576500); }
void Test013(void) { assert(FirstDigitsOfSum(
 "37107287533902102798797998220837590246510135740250"
 "46376937677490009712648124896970078050417018260538"
 "74324986199524741059474233309513058123726617309629"
 "91942213363574161572522430563301811072406154908250"
 "23067588207539346171171980310421047513778063246676"
 "89261670696623633820136378418383684178734361726757"
 "28112879812849979408065481931592621691275889832738"
 "44274228917432520321923589422876796487670272189318"
 "47451445736001306439091167216856844588711603153276"
 "70386486105843025439939619828917593665686757934951"
 "62176457141856560629502157223196586755079324193331"
 "64906352462741904929101432445813822663347944758178"
 "92575867718337217661963751590579239728245598838407"
 "58203565325359399008402633568948830189458628227828"
 "80181199384826282014278194139940567587151170094390"
 "35398664372827112653829987240784473053190104293586"
 "86515506006295864861532075273371959191420517255829"
 "71693888707715466499115593487603532921714970056938"
 "54370070576826684624621495650076471787294438377604"
 "53282654108756828443191190634694037855217779295145"
 "36123272525000296071075082563815656710885258350721"
 "45876576172410976447339110607218265236877223636045"
 "17423706905851860660448207621209813287860733969412"
 "81142660418086830619328460811191061556940512689692"
 "51934325451728388641918047049293215058642563049483"
 "62467221648435076201727918039944693004732956340691"
 "15732444386908125794514089057706229429197107928209"
 "55037687525678773091862540744969844508330393682126"
 "18336384825330154686196124348767681297534375946515"
 "80386287592878490201521685554828717201219257766954"
 "78182833757993103614740356856449095527097864797581"
 "16726320100436897842553539920931837441497806860984"
 "48403098129077791799088218795327364475675590848030"
 "87086987551392711854517078544161852424320693150332"
 "59959406895756536782107074926966537676326235447210"
 "69793950679652694742597709739166693763042633987085"
 "41052684708299085211399427365734116182760315001271"
 "65378607361501080857009149939512557028198746004375"
 "35829035317434717326932123578154982629742552737307"
 "94953759765105305946966067683156574377167401875275"
 "88902802571733229619176668713819931811048770190271"
 "25267680276078003013678680992525463401061632866526"
 "36270218540497705585629946580636237993140746255962"
 "24074486908231174977792365466257246923322810917141"
 "91430288197103288597806669760892938638285025333403"
 "34413065578016127815921815005561868836468420090470"
 "23053081172816430487623791969842487255036638784583"
 "11487696932154902810424020138335124462181441773470"
 "63783299490636259666498587618221225225512486764533"
 "67720186971698544312419572409913959008952310058822"
 "95548255300263520781532296796249481641953868218774"
 "76085327132285723110424803456124867697064507995236"
 "37774242535411291684276865538926205024910326572967"
 "23701913275725675285653248258265463092207058596522"
 "29798860272258331913126375147341994889534765745501"
 "18495701454879288984856827726077713721403798879715"
 "38298203783031473527721580348144513491373226651381"
 "34829543829199918180278916522431027392251122869539"
 "40957953066405232632538044100059654939159879593635"
 "29746152185502371307642255121183693803580388584903"
 "41698116222072977186158236678424689157993532961922"
 "62467957194401269043877107275048102390895523597457"
 "23189706772547915061505504953922979530901129967519"
 "86188088225875314529584099251203829009407770775672"
 "11306739708304724483816533873502340845647058077308"
 "82959174767140363198008187129011875491310547126581"
 "97623331044818386269515456334926366572897563400500"
 "42846280183517070527831839425882145521227251250327"
 "55121603546981200581762165212827652751691296897789"
 "32238195734329339946437501907836945765883352399886"
 "75506164965184775180738168837861091527357929701337"
 "62177842752192623401942399639168044983993173312731"
 "32924185707147349566916674687634660915035914677504"
 "99518671430235219628894890102423325116913619626622"
 "73267460800591547471830798392868535206946944540724"
 "76841822524674417161514036427982273348055556214818"
 "97142617910342598647204516893989422179826088076852"
 "87783646182799346313767754307809363333018982642090"
 "10848802521674670883215120185883543223812876952786"
 "71329612474782464538636993009049310363619763878039"
 "62184073572399794223406235393808339651327408011116"
 "66627891981488087797941876876144230030984490851411"
 "60661826293682836764744779239180335110989069790714"
 "85786944089552990653640447425576083659976645795096"
 "66024396409905389607120198219976047599490197230297"
 "64913982680032973156037120041377903785566085089252"
 "16730939319872750275468906903707539413042652315011"
 "94809377245048795150954100921645863754710598436791"
 "78639167021187492431995700641917969777599028300699"
 "15368713711936614952811305876380278410754449733078"
 "40789923115535562561142322423255033685442488917353"
 "44889911501440648020369068063960672322193204149535"
 "41503128880339536053299340368006977710650566631954"
 "81234880673210146739058568557934581403627822703280"
 "82616570773948327592232845941706525094512325230608"
 "22918802058777319719839450180888072429661980811197"
 "77158542502016545090413245809786882778948721859617"
 "72107838435069186155435662884062257473692284509516"
 "20849603980134001723930671666823555245252804609722"
 "53503534226472524250874054075591789781264330331690"
  , 100, 50, 10) == 5537376230); }
void Test014(void) { assert(LongestCollatzSequence(1000000) == 837799); }
void Test015(void) { assert(LatticePaths(20, 20) == 137846528820); }
void Test016(void) { assert(PowerDigitSum(2, 1000) == 1366); }
void Test017(void) { assert(NumberLetterCounts(1, 1000) == 21124);  }
void Test018(void) { assert(MaximumPathSum((char*[]){
               "75",
              "95 64",
             "17 47 82",
            "18 35 87 10",
           "20 04 82 47 65",
          "19 01 23 75 03 34",
         "88 02 77 73 07 63 67",
        "99 65 04 28 06 16 70 92",
       "41 41 26 56 83 40 80 70 33",
      "41 48 72 33 47 32 37 16 94 29",
     "53 71 44 65 25 43 91 52 97 51 14",
    "70 11 33 28 77 73 17 78 39 68 17 57",
   "91 71 52 38 17 14 91 43 58 50 27 29 48",
  "63 66 04 68 89 53 67 30 73 16 69 87 40 31",
 "04 62 98 27 23 09 70 98 73 93 38 53 60 04 23"
}, 15)== 1074); }
void Test019(void) { 
  assert(CountingSundays(&(date) { 1901, 0, 1 }, &(date) { 2001, 0, 1 }) == 171);
}
void Test020(void) { assert(FactorialDigitSum(100) == 648); }
void Test021(void) { assert(SumOfAmicableNumbers(10000) == 31626); }
void Test022(void) { assert(NamesScores("p022_names.txt") == 871198282); }
void Test023(void) { assert(SumOfNonAbundantIntegers(28123) == 4179871); }
void Test024(void) { assert(NthLexicographicPermutation(10, 1000000) == 2783915460); }
void Test025(void) { assert(NthDigitFibonacciNumberIndex(1000) == 4782); }
void Test026(void) { assert(LongestReciprocalCycle(1000) == 983);  }
void Test027(void) { assert(QuadraticPrimes(1000, 1000) == -59231); }
void Test028(void) { assert(SumOfDiagonals(1001) == 669171001); }
void Test029(void) { assert(DistinctPowers(2, 100, 2, 100) == 9183); }
void Test030(void) { /*assert(test == 443839); */ }
void Test031(void) { /*assert(test == 73682); */ }
void Test032(void) { /*assert(test == 45228); */ }
void Test033(void) { /*assert(test == 100); */ }
void Test034(void) { /*assert(test == 40730); */ }
void Test035(void) { /*assert(test == 55); */ }
void Test036(void) { /*assert(test == 872187); */ }
void Test037(void) { /*assert(test == 748317); */ }
void Test038(void) { /*assert(test == 932718654); */ }
void Test039(void) { /*assert(test == 840); */ }
void Test040(void) { /*assert(test == 210); */ }
void Test041(void) { /*assert(test == 7652413); */ }
void Test042(void) { /*assert(test == 162); */ }
void Test043(void) { /*assert(test == 16695334890); */ }
void Test044(void) { /*assert(test == 5482660); */ }
void Test045(void) { /*assert(test == 1533776805); */ }
void Test046(void) { /*assert(test == 5777); */ }
void Test047(void) { /*assert(test == 134043); */ }
void Test048(void) { /*assert(test == 9110846700); */ }
void Test049(void) { /*assert(test == 296962999629); */ }
void Test050(void) { /*assert(test == 997651); */ }
void Test051(void) { /*assert(test == 121313); */ }
void Test052(void) { /*assert(test == 142857); */ }
void Test053(void) { /*assert(test == 4075); */ }
void Test054(void) { /*assert(test == 376); */ }
void Test055(void) { /*assert(test == 249); */ }
void Test056(void) { /*assert(test == 972); */ }
void Test057(void) { /*assert(test == 153); */ }
void Test058(void) { /*assert(test == 26241); */ }
void Test059(void) { /*assert(test == 107359); */ }
void Test060(void) { /*assert(test == 26033); */ }
void Test061(void) { /*assert(test == 28684); */ }
void Test062(void) { /*assert(test == 127035954683); */ }
void Test063(void) { /*assert(test == 49); */ }
void Test064(void) { /*assert(test == 1322); */ }
void Test065(void) { /*assert(test == 272); */ }
void Test066(void) { /*assert(test == 661); */ }
void Test067(void) { /*assert(test == 7273); */ }
void Test068(void) { /*assert(test == 6531031914842725); */ }
void Test069(void) { /*assert(test == 510510); */ }
void Test070(void) { /*assert(test == 8319823); */ }
void Test071(void) { /*assert(test == 428570); */ }
void Test072(void) { /*assert(test == 303963552391); */ }
void Test073(void) { /*assert(test == 7295372); */ }
void Test074(void) { /*assert(test == 402); */ }
void Test075(void) { /*assert(test == 161667); */ }
void Test076(void) { /*assert(test == 190569291); */ }
void Test077(void) { /*assert(test == 71); */ }
void Test078(void) { /*assert(test == 55374); */ }
void Test079(void) { /*assert(test == 73162890); */ }
void Test080(void) { /*assert(test == 40886); */ }
void Test081(void) { /*assert(test == 427337); */ }
void Test082(void) { /*assert(test == 260324); */ }
void Test083(void) { /*assert(test == 425185); */ }
void Test084(void) { /*assert(test == 101524); */ }
void Test085(void) { /*assert(test == 2772); */ }
void Test086(void) { /*assert(test == 1818); */ }
void Test087(void) { /*assert(test == 1097343); */ }
void Test088(void) { /*assert(test == 7587457); */ }
void Test089(void) { /*assert(test == 743); */ }
void Test090(void) { /*assert(test == 1217); */ }
void Test091(void) { /*assert(test == 14234); */ }
void Test092(void) { /*assert(test == 8581146); */ }
void Test093(void) { /*assert(test == 1258); */ }
void Test094(void) { /*assert(test == 518408346); */ }
void Test095(void) { /*assert(test == 14316); */ }
void Test096(void) { /*assert(test == 24702); */ }
void Test097(void) { /*assert(test == 8739992577); */ }
void Test098(void) { /*assert(test == 18769); */ }
void Test099(void) { /*assert(test == 709); */ }
void Test100(void) { /*assert(test == 756872327473); */ }
void Test101(void) { /*assert(test == 37076114526); */ }
void Test102(void) { /*assert(test == 228); */ }
void Test104(void) { /*assert(test == 329468); */ }
void Test105(void) { /*assert(test == 73702); */ }
void Test107(void) { /*assert(test == 259679); */ }
void Test108(void) { /*assert(test == 180180); */ }
void Test109(void) { /*assert(test == 38182); */ }
void Test111(void) { /*assert(test == 612407567715); */ }
void Test112(void) { /*assert(test == 1587000); */ }
void Test113(void) { /*assert(test == 51161058134250); */ }
void Test114(void) { /*assert(test == 16475640049); */ }
void Test115(void) { /*assert(test == 168); */ }
void Test116(void) { /*assert(test == 20492570929); */ }
void Test117(void) { /*assert(test == 100808458960497); */ }
void Test118(void) { /*assert(test == 44680); */ }
void Test119(void) { /*assert(test == 248155780267521); */ }
void Test120(void) { /*assert(test == 333082500); */ }
void Test121(void) { /*assert(test == 2269); */ }
void Test122(void) { /*assert(test == 1582); */ }
void Test123(void) { /*assert(test == 21035); */ }
void Test124(void) { /*assert(test == 21417); */ }
void Test125(void) { /*assert(test == 2906969179); */ }
void Test127(void) { /*assert(test == 18407904); */ }
void Test128(void) { /*assert(test == 14516824220); */ }
void Test129(void) { /*assert(test == 1000023); */ }
void Test130(void) { /*assert(test == 149253); */ }
void Test132(void) { /*assert(test == 843296); */ }
void Test133(void) { /*assert(test == 453647705); */ }
void Test134(void) { /*assert(test == 18613426663617118); */ }
void Test135(void) { /*assert(test == 4989); */ }
void Test142(void) { /*assert(test == 1006193); */ }
void Test145(void) { /*assert(test == 608720); */ }
void Test146(void) { /*assert(test == 676333270); */ }
void Test149(void) { /*assert(test == 52852124); */ }
void Test150(void) { /*assert(test == -271248680); */ }
void Test151(void) { /*assert(test == 0.464399); */ }
void Test155(void) { /*assert(test == 3857447); */ }
void Test160(void) { /*assert(test == 16576); */ }
void Test162(void) { /*assert(test == "3D58725572C62302"); */ }
void Test164(void) { /*assert(test == 378158756814587); */ }
void Test166(void) { /*assert(test == 7130034); */ }
void Test169(void) { /*assert(test == 178653872807); */ }
void Test171(void) { /*assert(test == 142989277); */ }
void Test172(void) { /*assert(test == 227485267000992000); */ }
void Test173(void) { /*assert(test == 1572729); */ }
void Test174(void) { /*assert(test == 209566); */ }
void Test179(void) { /*assert(test == 986262); */ }
void Test182(void) { /*assert(test == 399788195976); */ }
void Test186(void) { /*assert(test == 2325629); */ }
void Test187(void) { /*assert(test == 17427258); */ }
void Test188(void) { /*assert(test == 95962097); */ }
void Test191(void) { /*assert(test == 1918080160); */ }
void Test197(void) { /*assert(test == 1.710637717); */ }
void Test203(void) { /*assert(test == 34029210557338); */ }
void Test204(void) { /*assert(test == 2944730); */ }
void Test205(void) { /*assert(test == 0.5731441); */ }
void Test206(void) { /*assert(test == 1389019170); */ }
void Test211(void) { /*assert(test == 1922364685); */ }
void Test214(void) { /*assert(test == 1677366278943); */ }
void Test216(void) { /*assert(test == 5437849); */ }
void Test218(void) { /*assert(test == 0); */ }
void Test222(void) { /*assert(test == 1590933); */ }
void Test225(void) { /*assert(test == 2009); */ }
void Test231(void) { /*assert(test == 7526965179680); */ }
void Test243(void) { /*assert(test == 892371480); */ }
void Test249(void) { /*assert(test == 9275262564250418); */ }
void Test250(void) { /*assert(test == 1425480602091519); */ }
void Test265(void) { /*assert(test == 209110240768); */ }
void Test267(void) { /*assert(test == 0.999992836187); */ }
void Test271(void) { /*assert(test == 4617456485273129588); */ }
void Test280(void) { /*assert(test == 430.088247); */ }
void Test301(void) { /*assert(test == 2178309); */ }
void Test303(void) { /*assert(test == 1111981904675169); */ }
void Test304(void) { /*assert(test == 283988410192); */ }
void Test315(void) { /*assert(test == 13625242); */ }
void Test323(void) { /*assert(test == 6.3551758451); */ }
void Test345(void) { /*assert(test == 13938); */ }
void Test357(void) { /*assert(test == 1739023853137); */ }
void Test381(void) { /*assert(test == 139602943319822); */ }
void Test387(void) { /*assert(test == 696067597313468); */ }
void Test401(void) { /*assert(test == 281632621); */ }
void Test407(void) { /*assert(test == 39782849136421); */ }
void Test417(void) { /*assert(test == 446572970925740); */ }
void Test425(void) { /*assert(test == 46479497324); */ }
void Test429(void) { /*assert(test == 98792821); */ }
void Test431(void) { /*assert(test == 23.386029052); */ }
void Test433(void) { /*assert(test == 326624372659664); */ }
void Test451(void) { /*assert(test == 153651073760956); */ }

void InitTests(void)
{
  Excercises[1] = Test001;
  Excercises[2] = Test002;
  Excercises[3] = Test003;
  Excercises[4] = Test004;
  Excercises[5] = Test005;
  Excercises[6] = Test006;
  Excercises[7] = Test007;
  Excercises[8] = Test008;
  Excercises[9] = Test009;
  Excercises[10] = Test010;
  Excercises[11] = Test011;
  Excercises[12] = Test012;
  Excercises[13] = Test013;
  Excercises[14] = Test014;
  Excercises[15] = Test015;
  Excercises[16] = Test016;
  Excercises[17] = Test017;
  Excercises[18] = Test018;
  Excercises[19] = Test019;
  Excercises[20] = Test020;
  Excercises[21] = Test021;
  Excercises[22] = Test022;
  Excercises[23] = Test023;
  Excercises[24] = Test024;
  Excercises[25] = Test025;
  Excercises[26] = Test026;
  Excercises[27] = Test027;
  Excercises[28] = Test028;
  Excercises[29] = Test029;
  Excercises[30] = Test030;
  Excercises[31] = Test031;
  Excercises[32] = Test032;
  Excercises[33] = Test033;
  Excercises[34] = Test034;
  Excercises[35] = Test035;
  Excercises[36] = Test036;
  Excercises[37] = Test037;
  Excercises[38] = Test038;
  Excercises[39] = Test039;
  Excercises[40] = Test040;
  Excercises[41] = Test041;
  Excercises[42] = Test042;
  Excercises[43] = Test043;
  Excercises[44] = Test044;
  Excercises[45] = Test045;
  Excercises[46] = Test046;
  Excercises[47] = Test047;
  Excercises[48] = Test048;
  Excercises[49] = Test049;
  Excercises[50] = Test050;
  Excercises[51] = Test051;
  Excercises[52] = Test052;
  Excercises[53] = Test053;
  Excercises[54] = Test054;
  Excercises[55] = Test055;
  Excercises[56] = Test056;
  Excercises[57] = Test057;
  Excercises[58] = Test058;
  Excercises[59] = Test059;
  Excercises[60] = Test060;
  Excercises[61] = Test061;
  Excercises[62] = Test062;
  Excercises[63] = Test063;
  Excercises[64] = Test064;
  Excercises[65] = Test065;
  Excercises[66] = Test066;
  Excercises[67] = Test067;
  Excercises[68] = Test068;
  Excercises[69] = Test069;
  Excercises[70] = Test070;
  Excercises[71] = Test071;
  Excercises[72] = Test072;
  Excercises[73] = Test073;
  Excercises[74] = Test074;
  Excercises[75] = Test075;
  Excercises[76] = Test076;
  Excercises[77] = Test077;
  Excercises[78] = Test078;
  Excercises[79] = Test079;
  Excercises[80] = Test080;
  Excercises[81] = Test081;
  Excercises[82] = Test082;
  Excercises[83] = Test083;
  Excercises[84] = Test084;
  Excercises[85] = Test085;
  Excercises[86] = Test086;
  Excercises[87] = Test087;
  Excercises[88] = Test088;
  Excercises[89] = Test089;
  Excercises[90] = Test090;
  Excercises[91] = Test091;
  Excercises[92] = Test092;
  Excercises[93] = Test093;
  Excercises[94] = Test094;
  Excercises[95] = Test095;
  Excercises[96] = Test096;
  Excercises[97] = Test097;
  Excercises[98] = Test098;
  Excercises[99] = Test099;
  Excercises[100] = Test100;
  Excercises[101] = Test101;
  Excercises[102] = Test102;
  Excercises[104] = Test104;
  Excercises[105] = Test105;
  Excercises[107] = Test107;
  Excercises[108] = Test108;
  Excercises[109] = Test109;
  Excercises[111] = Test111;
  Excercises[112] = Test112;
  Excercises[113] = Test113;
  Excercises[114] = Test114;
  Excercises[115] = Test115;
  Excercises[116] = Test116;
  Excercises[117] = Test117;
  Excercises[118] = Test118;
  Excercises[119] = Test119;
  Excercises[120] = Test120;
  Excercises[121] = Test121;
  Excercises[122] = Test122;
  Excercises[123] = Test123;
  Excercises[124] = Test124;
  Excercises[125] = Test125;
  Excercises[127] = Test127;
  Excercises[128] = Test128;
  Excercises[129] = Test129;
  Excercises[130] = Test130;
  Excercises[132] = Test132;
  Excercises[133] = Test133;
  Excercises[134] = Test134;
  Excercises[135] = Test135;
  Excercises[142] = Test142;
  Excercises[145] = Test145;
  Excercises[146] = Test146;
  Excercises[149] = Test149;
  Excercises[150] = Test150;
  Excercises[151] = Test151;
  Excercises[155] = Test155;
  Excercises[160] = Test160;
  Excercises[162] = Test162;
  Excercises[164] = Test164;
  Excercises[166] = Test166;
  Excercises[169] = Test169;
  Excercises[171] = Test171;
  Excercises[172] = Test172;
  Excercises[173] = Test173;
  Excercises[174] = Test174;
  Excercises[179] = Test179;
  Excercises[182] = Test182;
  Excercises[186] = Test186;
  Excercises[187] = Test187;
  Excercises[188] = Test188;
  Excercises[191] = Test191;
  Excercises[197] = Test197;
  Excercises[203] = Test203;
  Excercises[204] = Test204;
  Excercises[205] = Test205;
  Excercises[206] = Test206;
  Excercises[211] = Test211;
  Excercises[214] = Test214;
  Excercises[216] = Test216;
  Excercises[218] = Test218;
  Excercises[222] = Test222;
  Excercises[225] = Test225;
  Excercises[231] = Test231;
  Excercises[243] = Test243;
  Excercises[249] = Test249;
  Excercises[250] = Test250;
  Excercises[265] = Test265;
  Excercises[267] = Test267;
  Excercises[271] = Test271;
  Excercises[280] = Test280;
  Excercises[301] = Test301;
  Excercises[303] = Test303;
  Excercises[304] = Test304;
  Excercises[315] = Test315;
  Excercises[323] = Test323;
  Excercises[345] = Test345;
  Excercises[357] = Test357;
  Excercises[381] = Test381;
  Excercises[387] = Test387;
  Excercises[401] = Test401;
  Excercises[407] = Test407;
  Excercises[417] = Test417;
  Excercises[425] = Test425;
  Excercises[429] = Test429;
  Excercises[431] = Test431;
  Excercises[433] = Test433;
  Excercises[451] = Test451;
}