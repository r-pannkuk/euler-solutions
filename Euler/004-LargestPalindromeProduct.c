#include "EulerExcercises.h"
#include "Helper.h"

long LargestPalindromeProduct(unsigned digits)
{
  unsigned maxFactor = 1,
           minFactor = 1;

  for(unsigned i = 0; i < digits; ++i)
  {
    maxFactor = minFactor *= 10;
  }

  --maxFactor;
  minFactor /= 10;

  int n = maxFactor, 
      m = maxFactor;

  long largestPalindrome = 0;

  for(unsigned i = maxFactor; i > minFactor; --i)
  {
    for(unsigned j = maxFactor; j > minFactor; --j)
    {
      long test = i * j;

      if(IsPalindrome(test) && test > largestPalindrome)
        largestPalindrome = test;
    }
  }

  return largestPalindrome;
}


