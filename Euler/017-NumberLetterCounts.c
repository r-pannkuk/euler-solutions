#include "EulerExcercises.h"
#include "Helper.h"
#include <stdlib.h>
#include <math.h>

unsigned CountLetters(int n);
long long NumberLetterCountsFast(int upperBound);

static int ones[10] = {
  0, // Zero isn't said except on it's own
  3, // One
  3, // Two
  5, // Three
  4, // Four
  4, // Five
  3, // Six
  5, // Seven
  5, // Eight
  4, // Nine
};

static int tens[10] = {
  0, // Zero isn't said
  -1, // Special case for tens
  6, // Twenty
  6, // Thirty
  5, // Forty
  5, // Fifty
  5, // Sixty
  7, // Seventy
  6, // Eighty
  6, // Ninety
};

static int largeNumbers[4] = {
  0, // "(Not Said)"                   0 - 999
  8, // "Thousand"                  1000 - 999999
  7, // "Million"                1000000 - 999999999
  7, // "Billion"             1000000000 - 999999999999
};

long long NumberLetterCounts(int lowerBound, int upperBound)
{
  if(lowerBound == 1)
    return NumberLetterCountsFast(upperBound);

  long long count = 0;

  for(int n = lowerBound; n <= upperBound; ++n)
    count += CountLetters(n);

  return count;
}

long long NumberLetterCountsFast(int upperBound)
{
  long long count = 0;

  int sum1to9  = 3 + 3 + 5 + 4 + 4 + 3 + 5 + 5 + 4; // 36
  int sum10to19 = 3 + 6 + 6 + 8 + 8 + 7 + 7 + 9 + 8 + 8; // 70
  int sum20to99 = 10 * (6 + 6 + 5 + 5 + 5 + 7 + 6 + 6) + 8 * sum1to9; // 748
  int sum10to99 = sum10to19 + sum20to99;
  int sum1to99 = sum1to9 + sum10to19 + sum20to99; // 854
  int sum100to999 = sum1to9 * 100 + // X _______ ___ __
                    sum1to99 * 9 +  // _ _______ ___ YY
                    7 * 9 +         // _ hundred
                    10 * 9 * 99;    // _ hundred and __
  int sum1to999 = sum1to99 + sum100to999;
  
  int largeNumberTypes = 0;

  for(largeNumberTypes = 0; upperBound >= 1000; ++largeNumberTypes)
  {
    upperBound = upperBound / 1000;

    for(int j = 0; j <= largeNumberTypes; ++j)
      count += (long long)pow(1000, j) * sum1to999;
  }

  long long upperEndCount = 0;

  // UpperBound is now between 1 and 999
  for(int n = 1; n <= upperBound; ++n)
    upperEndCount += (CountLetters(n) + largeNumbers[largeNumberTypes]);

  return upperEndCount + count;
}

unsigned Ones(int n)
{
  return ones[n];
}

unsigned Tens(int n)
{
  unsigned count = 0;

  if(n >= 10 && n < 20)
  {
    switch(n)
    {
    case 10:
      count += 3;
      break;
    case 11:
      count += 6;
      break;
    case 12:
      count += 6;
      break;
    case 13:
      count += 8;
      break;
    case 14:
      count += 8;
      break;
    case 15:
      count += 7;
      break;
    case 16:
      count += 7;
      break;
    case 17:
      count += 9;
      break;
    case 18:
      count += 8;
      break;
    case 19:
      count += 8;
      break;
    }
  }
  else
  {
    count += tens[n / 10];  // ...Xty...

    count += Ones(n % 10);    // ... Y
  }

  return count;
}

unsigned Hundreds(int n)
{
  unsigned count = 0;
  
  if(n / 100 > 0)
  {
    count += Ones(n / 100) + 7; // Z Hundred ...

    if(n % 100 > 0)
      count += 3; // ... And ...
  }

  if(n % 100 > 0)
    count += Tens(n % 100); // ... Xty Y

  return count;
}

unsigned CountLetters(int n)
{
  unsigned largeNumberTypes;
  unsigned count = 0;

  for(largeNumberTypes = 0; n > 0; ++largeNumberTypes, n /= 1000)
    count += Hundreds(n % 1000);

  for(unsigned i = 0; i < largeNumberTypes; ++i)
    count += largeNumbers[i];

  return count;
}