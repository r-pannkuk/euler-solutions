#include "EulerExcercises.h"
#include "Helper.h"

long long Product(int* grid, int row, int col, int height,
                  int rowOffset, int colOffset, int numAdjacent);

long long LargestProductInGrid(int *grid, int width, int height, int numAdjacent)
{
  long long product;
  long long bestProduct = 0;

  for(int row = 0; row < height; ++row)
  {
    for(int col = 0; col < width; ++col)
    {

      // Right
      product = Product(grid, row, col, height, 0, 1, numAdjacent);
      if(product > bestProduct)
        bestProduct = product;

      // Down
      product = Product(grid, row, col, height, 1, 0, numAdjacent);
      if(product > bestProduct)
        bestProduct = product;

      // Diagonal SE
      product = Product(grid, row, col, height, 1, 1, numAdjacent);
      if(product > bestProduct)
        bestProduct = product;

      // Diagonal SW
      product = Product(grid, row, col, height, 1, -1, numAdjacent);
      if(product > bestProduct)
        bestProduct = product;
    }
  }

  return bestProduct;
}


long long Product(int* grid, int row, int col, int height, 
                  int rowOffset, int colOffset, int numAdjacent)
{
  long long product = 1;

  if(!(((row + (numAdjacent - 1)) < height) && ((row - (numAdjacent - 1)) >= 0) &&
      ((col + (numAdjacent - 1)) < height) && ((col + (numAdjacent - 1)) >= 0)))
    return 0;

  for(int i = 0; i < numAdjacent; ++i)
      product *= grid[(row + i * rowOffset) * height + (col + i * colOffset)];

  return product;
}