#include "EulerExcercises.h"
#include "Helper.h"
#include <stdlib.h>
#include <math.h>

long long RecursiveLatticePaths(unsigned int width, unsigned int height);
long long MemoizedRecursiveLatticePaths(unsigned int width, unsigned int height, 
                                        long long* cache, unsigned int originalWidth);

long long LatticePaths(unsigned int width, unsigned int height)
{
  /*long long* cache = (long long*)calloc((width + 1) * (height + 1), sizeof(long long));
  long long value = MemoizedRecursiveLatticePaths(width, height, cache, width);
  free(cache);*/

  long long value = Binomial((width + height), width);
  return value;
}

long long RecursiveLatticePaths(unsigned int width, unsigned int height)
{
  if(width == 0)
    return 1;
  if(height == 0)
    return 1;

  return RecursiveLatticePaths(width - 1, height) + RecursiveLatticePaths(width, height - 1);
}

long long MemoizedRecursiveLatticePaths(unsigned int width, unsigned int height, 
                                        long long* cache, unsigned int originalWidth)
{
  if(cache[height * originalWidth + width] != 0)
    return cache[height * originalWidth + width];
  if(width == 0)
    return 1;
  if(height == 0)
    return 1;

  cache[height * originalWidth + width] =
    MemoizedRecursiveLatticePaths(width - 1, height, cache, originalWidth) + 
    MemoizedRecursiveLatticePaths(width, height - 1, cache, originalWidth);

  return cache[height * originalWidth + width];
}