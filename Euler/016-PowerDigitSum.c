#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>

long long PowerDigitSum(int base, int exponent)
{
  large_number exponential = LargeNumber();
  exponential.d[0] = '1';
  exponential.numDigits = 1;

  for(int i = 0; i < exponent; ++i)
    exponential = Scalar(&exponential, base);

  unsigned sum = 0;

  for(int i = 0; i < exponential.numDigits; ++i)
    sum += exponential.d[i] - '0';

  return sum;
}