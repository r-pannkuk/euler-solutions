#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>

unsigned DistinctPowers(unsigned minA, unsigned maxA, unsigned minB, unsigned maxB)
{
  unsigned numFound = (maxA - minA + 1) * (maxB - minB + 1);
  char* foundPower = (char*)calloc(numFound, 1);
  char* counted = (char*)malloc((size_t)maxA * 1);

  unsigned highestExp;
  for(highestExp = 2; pow(minA, highestExp) <= maxA; ++highestExp);
  --highestExp;
  
  double root;
  unsigned power;

  for(unsigned exp = highestExp; exp >= 2; --exp)
  {
    for(unsigned a = minA; pow(a, exp) <= maxA; ++a)
    {
      power = (unsigned)pow(a, exp);

      if(foundPower[power])
        continue;

      counted = (char*)memset(counted, 0, maxA);

      for(unsigned i = 1; i < exp; ++i)
      {
        root = pow(power, (double)i / exp);
      
        if(abs(root - (double)(int)(root + 0.5)) < 0.0001)
        {
          unsigned percentile = i;
          unsigned step = i;

          if(exp % 2 == 0 && exp / i % 2 == 0)
          {
              percentile = exp / i / 2;
          }

          // Limited to exp of 8
          if(i == 4 && exp % 2 == 0)
            step = 2;

          unsigned start = (minA % i == 0) ? minA : minA + i - minA % i;

          for(unsigned j = start; j <= percentile * maxA / exp; j += step)
          {
            if(counted[j])
              continue;

            counted[j] = 1;
            --numFound;
          }
        }
      }

      foundPower[power] = 1;
    }
  }

  free(foundPower);

  return numFound;
}