#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>
#include <stdlib.h>

long long QuadraticPrimes(unsigned limitA, unsigned limitB)
{
  char* sieve = SieveOfEratosthenes(limitA * limitB);

  unsigned maxN = 0;
  int maxA = 0;
  int maxB = 0;

  unsigned n;

  // For n = 1, n^2 + an + b = 1 + a + b
  // Since all primes except for 2 have to be odd, so a + b has to be even
  int a = -1 * (int)limitA;
  if(a % 2 == 0)
    a += 1;

  // Because n^2 + an + b = b when n = 0, we only have to check primes
  

  for(; a <= (int)limitA; a += 2)
  {
    for(int b = 3; b <= (int)limitB; b += 2)
    {
      // Can skip on b not being prime, because n^2 + an + b = b when n = 0
      if(sieve[b] == 1)
        continue;

      // For n = 1, if a + b are odd, then we will get an even number
      if(a + b != 2 && abs(a) % 2 != b % 2)
        continue;

      n = 0;

      // n^2 + an + b
      while(sieve[abs(n * n + a * n + b)] == 0) {
        ++n;
      }

      if(n > maxN)
      {
        maxN = n;
        maxA = a;
        maxB = b;
      }
    }
  }

  free(sieve);

  return maxA * maxB;
}