#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>

long long SpecialPythagoreanTriplet(int n)
{
  // a^2 + b^2 = c^2
  // c = sqrt(a^2 + b^2)

  // a + b + c = n

  // a + b + sqrt(a^2 + b^2) = n
  // sqrt(a^2 + b^2) = n - a - b
  // a^2 + b^2 = (n - a - b)^2
  // a^2 + b^2 = a^2 + 2ab - 2an + b^2 - 2bn + n^2
  // 0 = 2ab - 2an - 2bn + n^2
  // 0 = b(2a - 2n) - 2an + n^2
  // b(2a - 2n) = 2an - n^2
  // b = (2an - n^2) / (2a - 2n)
  // b = 2n (a - n/2) / 2 (a - n)
  // b = n * (a - n/2) / (a - n)

  double b;

  // a + b + c = n, and c > b > a.  Therefore, b < n/2, a < n/3
  for(int a = 2; a <= n / 3; ++a)
  {
    b = n * (a - n / 2.0) / (double)(a - n);

    // Check to see if b is a whole number / an integer
    if(ceil(b) == b)
      return (long long)(a * b * sqrt(a * a + b * b));
  }

  return -1;
}