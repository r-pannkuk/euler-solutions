#include "EulerExcercises.h"

unsigned SumOfEvenFibonacci(unsigned limit)
{
  // Starting with 1 as opposed to 0
  // Requirement of excercise
  unsigned pre_previous = 1;
  unsigned previous = 2;

  // Starting with 2
  unsigned sumOfEvens = 2;

  while(previous < limit)
  {
    unsigned temp = previous;
    previous += pre_previous;
    pre_previous = temp;

    // Checking for evenness here
    if(previous % 2 == 0)
      sumOfEvens += previous;
  }

  return sumOfEvens;
}