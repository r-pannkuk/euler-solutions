#include "EulerExcercises.h"
#include "Helper.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Max name length set to 100 characters
#define MAX_NAME_LENGTH sizeof(char) * 100

void StringQuickSort(char** list, int l, int r);

long long NamesScores(char* filename)
{
  FILE* file;
  unsigned long length;
  char* buffer;
  char** names;
  unsigned numNames;
  long long sum = 0;

  // Read the file contents into a buffer
  {
    fopen_s(&file, filename, "r");

    fseek(file, 0, SEEK_END);
    length = ftell(file);
    fseek(file, 0, SEEK_SET);

    buffer = (char*)malloc(sizeof(char) * length + 1);

    fread(buffer, sizeof(char), length, file);

    fclose(file);
  }


  // Allocate and create names array
  {
    numNames = 0;

    for(unsigned long i = 0; i < length; ++i)
    {
      if(buffer[i] == ',')
        ++numNames;
    }

    // Final name will not have a comma at the end
    ++numNames;

    names = (char**)malloc(sizeof(char*) * numNames);

    int isName = 0;

    for(unsigned i = 0; i < numNames; ++i)
      names[i] = malloc(MAX_NAME_LENGTH);

    unsigned nameIndex = 0; 
    unsigned long i = 0;

    // Names will be in following format: 
    //    "MARTHA","STEVEN","PHILLIP",...,"BOBBY"

    while(i < length)
    {
      // Starting at a quotation mark
      ++i;

      unsigned currentNameIndex = 0;

      while(buffer[i] != '\"')
        names[nameIndex][currentNameIndex++] = buffer[i++];
      
      names[nameIndex][currentNameIndex] = '\0';

      // ...",
      i += 2;
      ++nameIndex;
    }
  }

  // Sort the array based on the names, alphabetically
  StringQuickSort(names, 0, numNames - 1);

  // Calculate the numerical values for every name and add to sum
  for(unsigned i = 0; i < numNames; ++i)
  {
    unsigned nameValue = 0;
    unsigned length = strlen(names[i]);

    for(unsigned j = 0; j < length; ++j)
      nameValue += names[i][j] - 'A' + 1;

    sum += nameValue * (i + 1);
  }

  // Cleanup
  {
    for(unsigned i = 0; i < numNames; ++i)
      free(names[i]);

    free(names);
    free(buffer);
  }

  return sum;
}




void StringQuickSort(char** list, int l, int r)
{
  unsigned pivot;
  int i;
  int j;

  if(l < r)
  {
    pivot = l;
    i = l;
    j = r;


    while(i < j)
    {
      while(strcmp(list[i], list[pivot]) <= 0 && i < r)
        ++i;

      while(strcmp(list[pivot], list[j]) < 0)
        --j;

      if(i < j)
      {
        char* temp = list[i];
        list[i] = list[j];
        list[j] = temp;
      }
    }

    char* temp = list[pivot];
    list[pivot] = list[j];
    list[j] = temp;
  
    StringQuickSort(list, l, j - 1);
    StringQuickSort(list, j + 1, r);
  }
}