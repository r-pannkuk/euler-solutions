#include "EulerExcercises.h"
#include "Helper.h"

long long SumSquareDifference(unsigned upperBound)
{
  // Can be calculated to: f(n) = n * (n + 1) / 2
  long long sum = upperBound * (upperBound + 1) / 2;

  // Can be calculated to: f(n) = n / 6 * (2n + 1) * (n + 1)
  long long sumOfSquares = (2 * upperBound + 1) * (upperBound + 1) * upperBound / 6;

  return sum * sum - sumOfSquares;
}