#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>

#define PHI 1.61803399

unsigned NthDigitFibonacciNumberIndex(unsigned numDigits)
{
  // Binet Formula:
  //   F(n) = [ PHI^n - (-1 / PHI)^n ] / sqrt(5);
  
  // Length of Number:
  //   D(p) = log_10(p) + 1;
  //   D(p) = ln(p) / ln(10) + 1;

  // Length of Fibonacci Term:
  //   D(n) = log_10([PHI^n - (-1 / PHI)^n] / sqrt(5)) + 1;

  // (-1 / PHI)^n is an extremely small number, so ignored

  // D(n) = n * log(PHI) - 0.5 * log(5) + 1
  // D(n) - 1 + 0.5 * log(5) = n * log(PHI)
  // n = (D(n) - 1 + 0.5 * log(5)) / log(PHI)

  return (unsigned)ceil((numDigits - 1 + 0.5 * log10(5)) / log10(PHI));
}