#include "EulerExcercises.h"
#include "Helper.h"

long long LargestProductInSeries(char* number, int length, int adjacentDigits)
{
  // Need to use large data types to store very large numbers
  // Could use doubles, but even they can have accuracy errors at
  // extremely large numbers.  Best case: use notation form. 
  long long product = 1;
  long long largestProduct = 0;
  int i = 0;

  // Obtain initial 
  for(i = 0; i < adjacentDigits; ++i)
    product *= CharToInt(number[i]);
                             
  for(; i < length; ++i)
  {
    if(product > largestProduct)
      largestProduct = product;

    // Divide out the 'oldest' factor to be replaced
    if(number[i - adjacentDigits] != '0')
      product /= CharToInt(number[i - adjacentDigits]);

    // Checking for zero, and in this case we need to recalculate to ensure
    // we have an accurate product for the next iteration
    else
    {
      product = 1;

      for(int j = 1; j < adjacentDigits; ++j)
        product *= CharToInt(number[i - adjacentDigits + j]);
    }

    // Multiply next part of the product series
    product *= CharToInt(number[i]);
  }

  return largestProduct;
}

