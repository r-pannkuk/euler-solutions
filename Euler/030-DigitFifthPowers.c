#include "EulerExcercises.h"
#include "Helper.h"
#include <math.h>

unsigned long long SumOfDigitPowers(unsigned exp)
{
  unsigned long long limit = (unsigned)pow(9, exp) * (exp - 1);
  unsigned long long sum = 0;

  for(unsigned long long i = 2; i < limit; ++i)
  {
    unsigned numDigits = NumDigits(i);
    unsigned digitSum = 0;

    for(unsigned j = 0; j < numDigits; ++j)
    {
      unsigned digit = NthDigit(i, j);

      digitSum += (unsigned)pow(digit, exp);
    }

    if(digitSum == i)
      sum += i;
  }

  return sum;
}