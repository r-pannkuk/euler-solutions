#include "EulerExcercises.h"
#include "Helper.h"
#include <stdlib.h>
#include <math.h>

long long FirstDigitsOfLargeNumber(large_number number, unsigned numDigits);
large_number* SplitStringIntoLargeNumbers(char* numberString, unsigned lines, unsigned digitsPerLine);

long long FirstDigitsOfSum(char* numberString, unsigned lines, unsigned digitsPerLine, unsigned numFirstDigits)
{
  large_number* split = SplitStringIntoLargeNumbers(numberString, lines, digitsPerLine);
  large_number value = LargeNumber();

  for(unsigned i = 0; i < lines; ++i)
    value = Add(&value, &split[i]);

  free(split);

  return FirstDigitsOfLargeNumber(value, numFirstDigits);
}

large_number* SplitStringIntoLargeNumbers(char* numberString, unsigned lines, unsigned digitsPerLine)
{
  large_number* split = (large_number *)malloc(sizeof(large_number) * lines);
  unsigned line;

  for(line = 0; line < lines - 1; ++line)
  {
    split[line] = StringToLargeNumber(numberString + line * digitsPerLine, digitsPerLine);
  }

  int endOfLine = digitsPerLine;

  while(numberString[(line)* digitsPerLine + endOfLine] != 0)
    --endOfLine;

  split[line] = StringToLargeNumber(numberString + line * digitsPerLine, endOfLine);

  return split;
}

long long FirstDigitsOfLargeNumber(large_number number, unsigned numDigits)
{
  unsigned long long value = 0;

  for(unsigned i = 0; i < numDigits; ++i)
  {
    value += (unsigned long long)pow(10, (numDigits - 1) - i) * (number.d[i] - '0');
  }

  return value;
}