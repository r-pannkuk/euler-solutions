#include "EulerExcercises.h"
#include "Helper.h"

unsigned long long SumOfDiagonals(unsigned long n)
{
  if(n == 1)
    return 1;

  // Let r be the ring number, such that n = 0 is 1 and n = 1 is [2,9]
  // f(r) = 4(2n + 1)^2 - 12n
  // f(r_all) = 4(2n + 1)^2 - 12n + f(r_all - 1)

  // Can be written as a third-order polynomial:
  // f(r_all) = 16/3r^3 + 10r^2 + 26/3r + 1
  
  unsigned long r = (n - 1) / 2;

  // In case of n == 3
  if(r == 0)
    return 25;

  unsigned long value = (16 * r * r * r + 30 * r * r + 26 * r) / 3 + 1;
  return value;
}

unsigned long long SumOfDiagonalsIterative(unsigned long n)
{
  unsigned long sum = 1;
  unsigned long step = 2;
  unsigned long i = 1;

  while(i < n * n)
  {
    for(int corners = 0; corners < 4; ++corners)
      sum += i += step;
    step += 2;
  }

  return sum;
}