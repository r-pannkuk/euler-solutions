#include "EulerExcercises.h"
#include "Helper.h"
#include <stdlib.h>

int CollatzSequenceLength(long long n);
int MemoizedCollatzSequenceLength(long long n, int* cache);

long long LongestCollatzSequence(long long limit)
{
  if(limit < 2)
    return 1;

  int bestLength = 0;
  long long best = 0;
  int* lengths = (int*)calloc((size_t)limit, sizeof(int));
  lengths[0] = 1;
  lengths[1] = 1;

  for(int i = 2; i < limit; ++i)
  {
    int length = MemoizedCollatzSequenceLength(i, lengths);

    if(length > bestLength)
    {
      bestLength = length;
      best = i;
    }
  }

  free(lengths);

  return best;
}

int CollatzSequenceLength(long long n)
{
  // Collatz Sequence: 
  //    n --> n/2 (n is even)
  //    n --> 3n + 1 (n is odd)
  // e.g. 13 --> 40 --> 20 --> 10 --> 5 --> 16 --> 8 --> 4 --> 2 --> 1

  int length = 1;

  while(n > 1)
  {
    if(n % 2 == 0)
      n /= 2;
    else
      n = 3 * n + 1;

    ++length;
  }

  return length;
}

int MemoizedCollatzSequenceLength(long long n, int* cache)
{
  long long start = n;
  int length = 0;

  while(n > 0)
  {
    // Need to check for bounds here.  This is a point where we could
    // use more memory for the sake of speed.
    if(n < start && cache[n] != 0)
      break;

    if(n % 2 == 0)
      n /= 2;
    else
      n = 3 * n + 1;

    ++length;
  }

  cache[start] = length + cache[n];

  return cache[start];
}