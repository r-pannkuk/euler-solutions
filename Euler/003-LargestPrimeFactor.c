#include <math.h>
#include "EulerExcercises.h"
#include "Helper.h"

long long LargestPrimeFactor(long long number)
{
  long long nextPrime = 2;

  // Bound to sqrt(n).  Example: 100 highest root would be 10x10, instead of 20x5, etc.
  long long root = (long long)ceil(sqrt((double)number));

  while(nextPrime <= root)
  {
    if(number % nextPrime == 0)
    {
      while(number % nextPrime == 0)
      {
        number /= nextPrime;
      }

      root = (long long)ceil(sqrt((double)number));
    }

    nextPrime = GetNextPrime(nextPrime);
  }

  return number;
}