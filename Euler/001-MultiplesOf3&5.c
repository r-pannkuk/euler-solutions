#include "EulerExcercises.h"

int NumMultiples(int firstValue, int secondValue, 
                 int lowerBound, int upperBound)
{
    int sum = 0;
        
    for(int i = lowerBound; i < upperBound; ++i)
    {
        if(i % firstValue == 0 || i % secondValue == 0)
        {
            sum += i;
        }
    }
    
    return sum;
}